package Controller;

import POJO.ClientConfigPojo;

import java.io.*;
import java.net.Socket;

public class Client {

    private Socket serverSocket = null;
    private InputStream inputSocket;
    private OutputStream outputSocket;
    private final int bufferSize = 4000;
    private final String host;
    private final int port;

    public Client(ClientConfigPojo clientConfigPojo)
    {
        this.host = clientConfigPojo.getHost();
        this.port = clientConfigPojo.getPort();
    }

    public void connect()
    {
        try {
            serverSocket = new Socket(host, port);
            inputSocket = serverSocket.getInputStream();
            outputSocket = serverSocket.getOutputStream();
        } catch (IOException e) {
            System.out.println("ERROR while connecting");
            e.printStackTrace();
            try {
                serverSocket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            } finally {
                System.exit(-3);
            }
        }
    }

    private void sendMessage(String command)
    {
        PrintStream outputSocketWriter = new PrintStream(new BufferedOutputStream(outputSocket));
        outputSocketWriter.print(command);
        outputSocketWriter.flush();
    }

    private String receiveMessage()
    {
        BufferedReader inputSocketReader = new BufferedReader(new InputStreamReader(inputSocket));
        char[] buffer = new char[bufferSize];
        try {
            int length = inputSocketReader.read(buffer, 0, buffer.length);
            String receivedMessage = (new String(buffer, 0, length));
            return receivedMessage;
        } catch (Exception e) {
            System.out.println("Error while receiving message from server");
            try {
                serverSocket.close();
                return "";
            } catch (IOException ioException) {
                ioException.printStackTrace();
            } finally {
                System.exit(-4);
            }
            return "";
        }
    }

    public String executeCommand(String command)
    {
        StringBuilder msg = new StringBuilder();
        sendMessage(command);

        boolean receiveMessageComplete = false;
        while(!receiveMessageComplete){
            String currentMsg = receiveMessage();
            if(currentMsg.equals("fin")){
                receiveMessageComplete = true;
            } else{
                msg.append(currentMsg);
                sendMessage("ack");
            }
        }
        return msg.toString();
    }
}

package Controller;

import Gui.*;
import POJO.ClientConfigPojo;

import javax.swing.*;
import java.awt.*;

public class GuiManager {
    public final Gui addNewReviewGui;
    public final Gui connectedGui;
    public final Gui getCategoryTreeGui;
    public final Gui getOffersGui;
    public final Gui getProductGui;
    public final Gui getProductsByCategoryPathGui;
    public final Gui getProductsGui;
    public final Gui getSimilarCheaperProductsGui;
    public final Gui getTopProductsGui;
    public final Gui getTrollsGui;
    public final Gui unconnectedGui;
    public final Gui viewsReviewGui;

    private Gui currentGui;

    protected final JFrame frame = new JFrame();

    public GuiManager(ClientConfigPojo clientConfigPojo, Client client)
    {
        this.addNewReviewGui = new AddNewReviewGui(this, client);
        this.connectedGui = new ConnectedGui(this, client);
        this.getCategoryTreeGui = new GetCategoryTreeGui(this, client);
        this.getOffersGui = new GetOffersGui(this, client);
        this.getProductGui = new GetProductGui(this, client);
        this.getProductsByCategoryPathGui = new GetProductsByCategoryPathGui(this, client);
        this.getProductsGui = new GetProductsGui(this, client);
        this.getSimilarCheaperProductsGui = new GetSimilarCheaperProductGui(this, client);
        this.getTopProductsGui = new GetTopProductsGui(this, client);
        this.getTrollsGui = new GetTrollsGui(this, client);
        this.unconnectedGui = new UnconnectedGui(this, client, clientConfigPojo);
        this.viewsReviewGui = new ViewReviewsGui(this, client);

        this.currentGui = unconnectedGui;
    }

    public void start()
    {
        frame.setSize(1500, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        unconnectedGui.addPanelToFrame(frame);
        frame.setVisible(true);
    }


    public void switchGui(Gui newGui)
    {
        currentGui.removePanelFromFrame(frame);
        currentGui = newGui;
        currentGui.addPanelToFrame(frame);
        SwingUtilities.updateComponentTreeUI(frame);
    }

    public void openResultFrame(String result){
        JFrame resultFrame = new JFrame();
        resultFrame.setSize(900, 600);
        resultFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JPanel panel = new JPanel();

        TextArea textArea = new TextArea(result,30,100);
        panel.add(textArea);
        resultFrame.add(panel);
        resultFrame.setVisible(true);
    }

}

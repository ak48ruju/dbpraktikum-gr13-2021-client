import Controller.Client;
import Controller.GuiManager;
import POJO.ClientConfigPojo;


public class Main {

    public static void main(String[] args) {
        ClientConfigPojo defaultClientConfig = new ClientConfigPojo(
                "localhost",
                50001,
                "postgres",
                "123456"
        );
        Client client = new Client(defaultClientConfig);
        client.connect();
        GuiManager guiManager = new GuiManager(defaultClientConfig, client);
        guiManager.start();
    }
}

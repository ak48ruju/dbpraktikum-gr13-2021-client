package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.SubmitStateState;
import UserInput.Commands.GetProductsByCategoryPathCommand;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class GetProductsByCategoryPathGui extends SubmitGui{
    private JTextField categoryPathField;

    public GetProductsByCategoryPathGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
        this.submitStateInputs = new SubmitStateState(new GetProductsByCategoryPathCommand());
        configurePanel();
    }

    @Override
    protected Map<String, Object> generateArgumentsMap()
    {
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("Path", categoryPathField.getText());
        return arguments;
    }

    @Override
    protected void addActionListenerToInputFields(ActionListener actionListener)
    {
        categoryPathField.addActionListener(actionListener);
    }

    @Override
    protected void configurePanel()
    {
        categoryPathField = addLabelAndTextFieldToPanel("Category path", "Formate/Box-Sets/Blues");
        addSubmitAndReturnButtonToPanel();
    }
}

package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.SubmitStateState;
import UserInput.Commands.GetTrollsCommand;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class GetTrollsGui extends SubmitGui {


    private JTextField ratingField;
    private JTextField minimumAmountField;

    public GetTrollsGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
        this.submitStateInputs = new SubmitStateState(new GetTrollsCommand());
        configurePanel();
    }

    @Override
    protected Map<String, Object> generateArgumentsMap()
    {
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("Rating", ratingField.getText());
        arguments.put("MinimumAmount", minimumAmountField.getText());
        return arguments;
    }

    @Override
    protected void addActionListenerToInputFields(ActionListener actionListener)
    {
        ratingField.addActionListener(actionListener);
        minimumAmountField.addActionListener(actionListener);
    }

    @Override
    protected void configurePanel()
    {
        ratingField = addLabelAndTextFieldToPanel("Maximum average rating of user", "3");
        minimumAmountField = addLabelAndTextFieldToPanel("Minimum amount of reviews per user", "1");
        addSubmitAndReturnButtonToPanel();
    }
}

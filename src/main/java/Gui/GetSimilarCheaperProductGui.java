package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.SubmitStateState;
import UserInput.Commands.GetSimilarCheaperProductsCommand;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class GetSimilarCheaperProductGui extends SubmitGui{
    private JTextField asinField;

    public GetSimilarCheaperProductGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
        this.submitStateInputs = new SubmitStateState(new GetSimilarCheaperProductsCommand());
        configurePanel();
    }

    @Override
    protected Map<String, Object> generateArgumentsMap()
    {
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("Asin", asinField.getText());
        return arguments;
    }

    @Override
    protected void addActionListenerToInputFields(ActionListener actionListener)
    {
        asinField.addActionListener(actionListener);
    }

    @Override
    protected void configurePanel()
    {
        asinField = addLabelAndTextFieldToPanel("Product asin", "B0004FRW0C");
        addSubmitAndReturnButtonToPanel();
    }
}

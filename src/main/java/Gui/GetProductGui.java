package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.SubmitStateState;
import UserInput.Commands.GetProductCommand;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class GetProductGui extends SubmitGui {
    private JTextField asinField;

    public GetProductGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
        this.submitStateInputs = new SubmitStateState(new GetProductCommand());
        configurePanel();
    }

    @Override
    protected Map<String, Object> generateArgumentsMap()
    {
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("Asin", asinField.getText());
        return arguments;
    }

    @Override
    protected void addActionListenerToInputFields(ActionListener actionListener)
    {
        asinField.addActionListener(actionListener);
    }

    @Override
    protected void configurePanel()
    {
        asinField = addLabelAndTextFieldToPanel("Product Asin", "B0000668PG");
        addSubmitAndReturnButtonToPanel();
    }
}

package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.SubmitStateState;
import UserInput.Commands.GetCategoryTreeCommand;

import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class GetCategoryTreeGui extends SubmitGui {

    public GetCategoryTreeGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
        this.submitStateInputs = new SubmitStateState(new GetCategoryTreeCommand());
        configurePanel();
    }

    @Override
    protected Map<String, Object> generateArgumentsMap()
    {
        HashMap<String, Object> arguments = new HashMap<>();

        return arguments;
    }

    @Override
    protected void addActionListenerToInputFields(ActionListener actionListener)
    {

    }

    @Override
    protected void configurePanel()
    {
        addSubmitAndReturnButtonToPanel();
    }
}

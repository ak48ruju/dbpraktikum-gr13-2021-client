package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.SubmitStateInputs;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.Map;

public abstract class SubmitGui extends Gui {

    protected SubmitStateInputs submitStateInputs;

    public SubmitGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
    }

    protected void addSubmitAndReturnButtonToPanel()
    {
        JButton submitButton = addButtonToPanel("Submit");
        JButton returnButton = addButtonToPanel("Return");
        submitButton.addActionListener(generateActionListenerSubmitEvent());
        returnButton.addActionListener(actionEvent -> submitStateInputs.getReturnToConnectedGui().execute(guiManager, null));

    }

    private ActionListener generateActionListenerSubmitEvent()
    {
        ActionListener actionListener = actionEvent -> {
            this.submitStateInputs.getSubmitCommand().setArguments(generateArgumentsMap());
            this.submitStateInputs.getSubmitCommand().execute(this.guiManager, client);
        };
        addActionListenerToInputFields(actionListener);
        return actionListener;
    }

    ;

    protected abstract Map<String, Object> generateArgumentsMap();

    protected abstract void addActionListenerToInputFields(ActionListener actionListener);


}

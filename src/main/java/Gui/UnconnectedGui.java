package Gui;

import Controller.Client;
import Controller.GuiManager;
import POJO.ClientConfigPojo;
import States.UnconnectedInputs;
import States.UnconnectedState;
import UserInput.Commands.Command;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class UnconnectedGui extends Gui {

    private final UnconnectedInputs unconnectedInputs;
    private final ClientConfigPojo clientConfigPojo;
    private JTextField dbUserName;
    private JPasswordField dbPassword;

    public UnconnectedGui(GuiManager guiManager, Client client, ClientConfigPojo clientConfigPojo)
    {
        super(guiManager,client);
        this.unconnectedInputs = new UnconnectedState(clientConfigPojo);
        this.clientConfigPojo = clientConfigPojo;
        configurePanel();
    }



    protected void configurePanel(){


        dbUserName = addLabelAndTextFieldToPanel("Username database", clientConfigPojo.getDbName());
        dbPassword = addLabelAndPasswordFieldToPanel("Password database", clientConfigPojo.getDbPassword());
        JButton initButton = addButtonToPanel("Login");
        JButton quitButton = addButtonToPanel("Quit");



        ActionListener login = generateActionListener(unconnectedInputs.getInitCommand(), true);
        ActionListener quit = generateActionListener(unconnectedInputs.getQuitCommand(), false);

        dbUserName.addActionListener(login);
        dbPassword.addActionListener(login);

        initButton.addActionListener(login);
        quitButton.addActionListener(quit);

    }


    private Map<String, Object> generateArgumentsMapLogin(){
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("DbUser", dbUserName.getText());
        arguments.put("DbPass", String.valueOf(dbPassword.getPassword()));
        return arguments;
    }

    private ActionListener generateActionListener(Command command, boolean hasLoginArgs)
    {
        return actionEvent -> {
            final Map<String, Object> arguments = (hasLoginArgs)? generateArgumentsMapLogin() : new HashMap<>();
            command.setArguments(arguments);
            command.execute(this.guiManager, client);
        };
    }

}

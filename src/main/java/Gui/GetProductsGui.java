package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.SubmitStateState;
import UserInput.Commands.GetProductsCommand;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class GetProductsGui extends SubmitGui{
    private JTextField patternField;

    public GetProductsGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
        this.submitStateInputs = new SubmitStateState(new GetProductsCommand());
        configurePanel();
    }

    @Override
    protected Map<String, Object> generateArgumentsMap()
    {
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("Pattern", patternField.getText());
        return arguments;
    }

    @Override
    protected void addActionListenerToInputFields(ActionListener actionListener)
    {
        patternField.addActionListener(actionListener);
    }

    @Override
    protected void configurePanel()
    {
        patternField = addLabelAndTextFieldToPanel("Pattern", "Police");
        addSubmitAndReturnButtonToPanel();
    }
}

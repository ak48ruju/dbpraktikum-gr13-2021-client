package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.SubmitStateState;
import UserInput.Commands.GetTopProductsCommand;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class GetTopProductsGui extends SubmitGui{
    private JTextField kField;

    public GetTopProductsGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
        this.submitStateInputs = new SubmitStateState(new GetTopProductsCommand());
        configurePanel();
    }

    @Override
    protected Map<String, Object> generateArgumentsMap()
    {
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("K", kField.getText());
        return arguments;
    }

    @Override
    protected void addActionListenerToInputFields(ActionListener actionListener)
    {
        kField.addActionListener(actionListener);
    }

    @Override
    protected void configurePanel()
    {
        kField = addLabelAndTextFieldToPanel("Top k products to retrieve", "2");
        addSubmitAndReturnButtonToPanel();
    }
}

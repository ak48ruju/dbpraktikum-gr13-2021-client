package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.SubmitStateState;
import UserInput.Commands.ViewReviewsCommand;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class ViewReviewsGui extends SubmitGui{
    private JTextField asinField;

    public ViewReviewsGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
        this.submitStateInputs = new SubmitStateState(new ViewReviewsCommand());
        configurePanel();
    }

    @Override
    protected Map<String, Object> generateArgumentsMap()
    {
        HashMap<String, Object> arguments = new HashMap<>();
        arguments.put("Asin", asinField.getText());
        return arguments;
    }

    @Override
    protected void addActionListenerToInputFields(ActionListener actionListener)
    {
        asinField.addActionListener(actionListener);
    }

    @Override
    protected void configurePanel()
    {
        asinField = addLabelAndTextFieldToPanel("Product Asin", "B00005RSCH");
        addSubmitAndReturnButtonToPanel();
    }
}

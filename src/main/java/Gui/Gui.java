package Gui;

import Controller.Client;
import Controller.GuiManager;

import javax.swing.*;

public abstract class Gui {
    protected final JPanel panel = new JPanel();
    protected final GuiManager guiManager;
    private int nextComponentHeight = 10;
    protected Client client;


    public Gui(GuiManager guiManager, Client client){

        this.guiManager = guiManager;
        this.client = client;
        initPanel();
    }

    public void addPanelToFrame(JFrame frame){
        frame.add(panel);
    }
    public void removePanelFromFrame(JFrame frame){
        frame.remove(panel);
    }

    protected JButton addButtonToPanel(String name) {
        JButton button = new JButton(name);
        configureComponent(button);
        return button;
    }
    protected JLabel addLabelToPanel(String name) {
        JLabel label = new JLabel(name);
        configureComponent(label);
        return label;
    }
    protected JTextField addLabelAndTextFieldToPanel(String nameLabel, String textFieldDefaultValue) {
        JLabel label = new JLabel(nameLabel);
        configureComponent(label);
        JTextField textField = new JTextField(textFieldDefaultValue);
        configureComponent(textField, 215);
        return textField;
    }
    protected JPasswordField addLabelAndPasswordFieldToPanel(String name, String passwordFieldDefaultValue) {
        JLabel label = new JLabel(name);
        configureComponent(label);
        JPasswordField passwordField = new JPasswordField(passwordFieldDefaultValue);
        configureComponent(passwordField,215);
        return passwordField;
    }

    private void configureComponent(JComponent jComponent) {
        nextComponentHeight +=30;
        jComponent.setBounds(10, nextComponentHeight,200,25);
        panel.add(jComponent);
    }
    private void configureComponent(JComponent jComponent, int x) {
        jComponent.setBounds(x, nextComponentHeight,200,25);
        panel.add(jComponent);

    }



    protected void initPanel(){
        panel.setLayout(null);
    }

    protected abstract void configurePanel();
}

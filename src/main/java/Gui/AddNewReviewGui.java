package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.SubmitStateState;
import UserInput.Commands.AddNewReviewCommand;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class AddNewReviewGui extends SubmitGui {
    private JTextField usernameField;
    private JTextField asinField;
    private JTextField ratingField;
    private JTextField summaryField;
    private JTextField contentReviewField;

    public AddNewReviewGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
        this.submitStateInputs = new SubmitStateState(new AddNewReviewCommand());
        configurePanel();
    }

    @Override
    protected void configurePanel()
    {
        usernameField = addLabelAndTextFieldToPanel("Username", "adrian");
        asinField = addLabelAndTextFieldToPanel("Product asin", "B0000668PG");
        ratingField = addLabelAndTextFieldToPanel("Rating", "3");
        summaryField = addLabelAndTextFieldToPanel("Summary", "nice film");
        contentReviewField = addLabelAndTextFieldToPanel("Content review", "i really liked it");
        addSubmitAndReturnButtonToPanel();
    }

    @Override
    protected Map<String, Object> generateArgumentsMap()
    {
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("Username", usernameField.getText());
        arguments.put("Asin", asinField.getText());
        arguments.put("Rating", ratingField.getText());
        arguments.put("Summary", summaryField.getText());
        arguments.put("ContentReview", contentReviewField.getText());
        return arguments;
    }

    @Override
    protected void addActionListenerToInputFields(ActionListener actionListener)
    {
        usernameField.addActionListener(actionListener);
        asinField.addActionListener(actionListener);
        ratingField.addActionListener(actionListener);
        summaryField.addActionListener(actionListener);
        contentReviewField.addActionListener(actionListener);
    }

}

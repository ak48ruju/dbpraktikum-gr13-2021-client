package Gui;

import Controller.Client;
import Controller.GuiManager;
import States.ConnectedInputs;
import States.ConnectedState;

import javax.swing.*;

public class ConnectedGui extends Gui {

    private final ConnectedInputs connectedInputs;

    public ConnectedGui(GuiManager guiManager, Client client)
    {
        super(guiManager, client);
        this.connectedInputs = new ConnectedState();
        configurePanel();
    }

    protected void configurePanel()
    {
        JButton addNewReviewButton              = addButtonToPanel("AddNewReview");
        JButton getCategoryTreeButton           = addButtonToPanel("GetCategoryTree");
        JButton getOffersButton                 = addButtonToPanel("GetOffers");
        JButton getProductButton                = addButtonToPanel("GetProduct");
        JButton getProductsByCategoryPathButton = addButtonToPanel("GetProductsByCategoryPath");
        JButton getProductsButton               = addButtonToPanel("GetProducts");
        JButton getSimilarCheaperProductsButton = addButtonToPanel("GetSimilarCheaperProducts");
        JButton getTopProductsButton            = addButtonToPanel("GetTopProducts");
        JButton getTrollsButton                 = addButtonToPanel("GetTrolls");
        JButton viewReviewsButton               = addButtonToPanel("ViewsReview");
        JButton QuitButton                      = addButtonToPanel("Quit");

        addNewReviewButton              .addActionListener(actionEvent -> connectedInputs.goToAddNewReviewGui().execute(guiManager, null));
        getCategoryTreeButton           .addActionListener(actionEvent -> connectedInputs.goToGetCategoryTreeGui().execute(guiManager, null));
        getOffersButton                 .addActionListener(actionEvent -> connectedInputs.goToGetOffersGui().execute(guiManager, null));
        getProductButton                .addActionListener(actionEvent -> connectedInputs.goToGetProductGui().execute(guiManager, null));
        getProductsByCategoryPathButton .addActionListener(actionEvent -> connectedInputs.goToGetProductsByCategoryPathGui().execute(guiManager, null));
        getProductsButton               .addActionListener(actionEvent -> connectedInputs.goToGetProductsGui().execute(guiManager, null));
        getSimilarCheaperProductsButton .addActionListener(actionEvent -> connectedInputs.goToGetSimilarCheaperProductsGui().execute(guiManager, null));
        getTopProductsButton            .addActionListener(actionEvent -> connectedInputs.goToGetTopProductsGui().execute(guiManager, null));
        getTrollsButton                 .addActionListener(actionEvent -> connectedInputs.goToGetTrollsGui().execute(guiManager, null));
        viewReviewsButton               .addActionListener(actionEvent -> connectedInputs.goToViewReviewGui().execute(guiManager, null));

        QuitButton                      .addActionListener(actionEvent -> connectedInputs.getQuitCommand().execute(guiManager, client));


    }

}

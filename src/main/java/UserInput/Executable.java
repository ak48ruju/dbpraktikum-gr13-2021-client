package UserInput;

import Controller.Client;
import Controller.GuiManager;

public interface Executable {

    void execute(GuiManager guiManager, Client client);

}

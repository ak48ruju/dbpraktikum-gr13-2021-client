package UserInput.Commands;

import Controller.GuiManager;

/**
 * Für eine bestimmte Produkt-Id werden mit dieser
 * Methode die Detailinformationen des Produkts ermittelt.
 */
public class GetProductCommand extends Command{
    public GetProductCommand()
    {
        super("getProduct");
    }
}

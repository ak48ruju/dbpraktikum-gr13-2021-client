package UserInput.Commands;

import Controller.GuiManager;

/**
 * Für das übergegebene Produkt(Id) werden alle verfügbaren Angebote zurückgeliefert.
 */
public class GetOffersCommand extends Command{
    public GetOffersCommand()
    {
        super("getOffers");
    }
}

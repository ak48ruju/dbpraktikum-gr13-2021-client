package UserInput.Commands;

import UserInput.Executable;
import Controller.GuiManager;

/**
 * Diese Methode liefert für ein Produkt(Id) eine List von Produkten, die ähnlich und billiger sind als das spezifizierte.
 */
public class GetSimilarCheaperProductsCommand extends Command implements Executable {
    public GetSimilarCheaperProductsCommand()
    {
        super("getSimilarCheaperProducts");
    }

}

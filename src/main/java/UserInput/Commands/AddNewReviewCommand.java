package UserInput.Commands;

import UserInput.Executable;
import Controller.GuiManager;

import java.util.Map;

/**
 * Die Rahmenapplikation erlaubt sowohl das Ansehen als auch Hinzufügen von Reviews.
 * MIt Hilfe der Methode wird ein neues Review in der Datenbank gespeichert.
 * <p>
 * --- only add new review here
 */
public class AddNewReviewCommand extends Command {
    public AddNewReviewCommand()
    {
        super("addNewReview");
    }
}

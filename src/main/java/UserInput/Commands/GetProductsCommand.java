package UserInput.Commands;


/**
 * Diese Methode soll eine Liste der in der Datenbank enthaltenen Produkte, deren Titel mit dem übergebenen Pattern
 * übereinstimmen, zurückliefern. Beachten Sie, dass im Falle von pattern=null die komplette Liste zurückgeliefert wird.
 * Das Pattern kann SQL-Wildcards enthalten.
 * Hinweis: der Patternvergleich kann mittels des SQL-Operators like durchgeführt werden.
 */
public class GetProductsCommand extends Command{
    public GetProductsCommand()
    {
        super("getProducts");
    }
}

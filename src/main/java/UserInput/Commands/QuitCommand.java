package UserInput.Commands;

import Controller.Client;
import Controller.GuiManager;

/**
 * Damit die Mittelschicht alle Ressourcen kontrolliert wieder freigeben kann, wird diese
 * Methode bei Beendigung der Anwendung aufgerufen. Hier sollten speziell die Datenbankobjekte
 * wieder freigegeben werden.
 */
public class QuitCommand extends Command {
    public QuitCommand()
    {
        super("finish");
    }

    @Override
    public void execute(GuiManager guiManager, Client client)
    {
        super.execute(guiManager, client);
        System.exit(2);
    }
}

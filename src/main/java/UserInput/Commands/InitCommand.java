package UserInput.Commands;

import Controller.Client;
import Controller.GuiManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

public class InitCommand extends Command {


    public InitCommand()
    {
        super("init");
    }

    @Override
    public void execute(GuiManager guiManager, Client client)
    {
        super.execute(guiManager, client);
        if (initWasSuccessful()) {
            guiManager.switchGui(guiManager.connectedGui);
        }
    }

    private boolean initWasSuccessful()
    {
        try {
            HashMap resultMap = new ObjectMapper().readValue(result, HashMap.class);
            String resultError = (String) resultMap.get("Error");
            return resultError.equals("");
        } catch (JsonProcessingException e) {
            return false;
        }
    }
}

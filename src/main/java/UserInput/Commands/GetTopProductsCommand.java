package UserInput.Commands;

import Controller.GuiManager;

/**
 * Diese Methode liefert eine Liste aller Produkte zurück, die unter den Top k sind basierend auf dem Rating.
 */
public class GetTopProductsCommand extends Command{

    public GetTopProductsCommand()
    {
        super("getTopProducts");
    }
}

package UserInput.Commands;

import UserInput.Executable;
import Controller.GuiManager;

/**
 * Diese Methode ermittelt den kompletten Kategorienbaum durch Rückgabe des Wurzelknotens.
 * Jeder Knoten ist dabei vom Typ Category und kann eine Liste von Unterknoten (d.h. Unterkategorien)
 * enthalten.
 */
public class GetCategoryTreeCommand extends Command {
    public GetCategoryTreeCommand()
    {
        super("getCategoryTree");
    }

}

package UserInput.Commands;

import Controller.GuiManager;

/**
 * Die Methode soll eine Liste von Nutzern ausgeben, deren Durchschnittsbewertung unter einem spezifizierten Rating ist.
 */
public class GetTrollsCommand extends Command{

    public GetTrollsCommand()
    {
        super("getTrolls");
    }
}

package UserInput.Commands;

import UserInput.Executable;
import Controller.GuiManager;

/**
 * Nach Angabe einer Kategorie (definiert durch den Pfad von der Wurzel zu sich selbst) soll die Liste der zugeordneten
 * Produkte ermittelt werden. Die Angabe des Pfades ist notwendig, da der Kategorienname allein nicht eindeutig ist.
 */
public class GetProductsByCategoryPathCommand extends Command {
    public GetProductsByCategoryPathCommand()
    {
        super("getProductsByCategoryPath");
    }


}

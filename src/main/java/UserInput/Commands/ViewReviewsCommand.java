package UserInput.Commands;

import Controller.GuiManager;

/**
 * Die Rahmenapplikation erlaubt sowohl das Ansehen als auch Hinzufügen von Reviews.
 * MIt Hilfe der Methode wird ein neues Review in der Datenbank gespeichert.
 *
 * -- only view review here
 */
public class ViewReviewsCommand extends Command{
    public ViewReviewsCommand()
    {
        super("viewReviews");
    }


}

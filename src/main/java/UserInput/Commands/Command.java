package UserInput.Commands;

import Controller.Client;
import Controller.GuiManager;
import UserInput.Executable;
import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public abstract class Command implements Executable {

    protected final String command;
    protected Map<String, Object> arguments = new HashMap<>();
    protected String result;


    public Command(String command)
    {
        this.command = command;
    }

    public JSONObject constructJSON()
    {
        JSONObject jsonCommand = new JSONObject();
        jsonCommand.put("Command", command);
        jsonCommand.put("Arguments", arguments);
        return jsonCommand;
    }

    public String getCommand()
    {
        return command;
    }

    public void setArguments(Map<String, Object> arguments){
        this.arguments = arguments;
    }

    @Override
    public void execute(GuiManager guiManager, Client client){
        System.out.println(
                constructJSON().toString()
        );
        result = client.executeCommand(constructJSON().toString());
        guiManager.openResultFrame(convertJsonStringToPrettyJsonString(result));
    }

    private String convertJsonStringToPrettyJsonString(String jsonString){
        Gson GSON = new GsonBuilder().setPrettyPrinting().create();
        String returnJson = GSON.toJson(JsonParser.parseString(jsonString));
        return returnJson;
    }


}

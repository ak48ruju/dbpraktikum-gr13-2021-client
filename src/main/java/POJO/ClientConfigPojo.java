package POJO;

public class ClientConfigPojo {

    private final String host;
    private final int port;

    private final String dbName;

    private final String dbPassword;

    public ClientConfigPojo(String host, int port, String dbName, String dbPassword)
    {
        this.host = host;
        this.port = port;
        this.dbName = dbName;
        this.dbPassword = dbPassword;
    }

    public String getDbName()
    {
        return dbName;
    }

    public String getDbPassword()
    {
        return dbPassword;
    }

    public String getHost()
    {
        return host;
    }



    public int getPort()
    {
        return port;
    }


}

package States;

import Gui.Gui;
import UserInput.Executable;

public interface ConnectedInputs {

    Executable goToAddNewReviewGui();
    Executable goToGetCategoryTreeGui();
    Executable goToGetOffersGui();
    Executable goToGetProductGui();
    Executable goToGetProductsByCategoryPathGui();
    Executable goToGetProductsGui();
    Executable goToGetSimilarCheaperProductsGui();
    Executable goToGetTopProductsGui();
    Executable goToGetTrollsGui();
    Executable goToViewReviewGui();
    Executable getQuitCommand();
}

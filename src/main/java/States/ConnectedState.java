package States;

import UserInput.Commands.QuitCommand;
import UserInput.Executable;

public class ConnectedState implements ConnectedInputs {
    private final QuitCommand quitCommand = new QuitCommand();

    @Override
    public Executable goToAddNewReviewGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.addNewReviewGui);
    }

    @Override
    public Executable goToGetCategoryTreeGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.getCategoryTreeGui);
    }

    @Override
    public Executable goToGetOffersGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.getOffersGui);
    }

    @Override
    public Executable goToGetProductGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.getProductGui);
    }

    @Override
    public Executable goToGetProductsByCategoryPathGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.getProductsByCategoryPathGui);
    }

    @Override
    public Executable goToGetProductsGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.getProductsGui);
    }

    @Override
    public Executable goToGetSimilarCheaperProductsGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.getSimilarCheaperProductsGui);
    }

    @Override
    public Executable goToGetTopProductsGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.getTopProductsGui);
    }

    @Override
    public Executable goToGetTrollsGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.getTrollsGui);
    }

    @Override
    public Executable goToViewReviewGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.viewsReviewGui);
    }


    @Override
    public Executable getQuitCommand()
    {
        return quitCommand;
    }
}

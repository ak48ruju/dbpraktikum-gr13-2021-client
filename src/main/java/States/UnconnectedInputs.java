package States;

import UserInput.Commands.Command;
import UserInput.Executable;

public interface UnconnectedInputs {

    Command getInitCommand();
    Command getQuitCommand();
}

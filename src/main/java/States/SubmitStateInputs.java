package States;


import UserInput.Commands.Command;
import UserInput.Executable;

public interface SubmitStateInputs {

    Command getSubmitCommand();
    Executable getReturnToConnectedGui();
}

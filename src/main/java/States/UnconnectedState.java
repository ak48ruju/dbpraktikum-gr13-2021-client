package States;

import UserInput.Commands.Command;
import UserInput.Commands.InitCommand;
import UserInput.Commands.QuitCommand;
import POJO.ClientConfigPojo;


public class UnconnectedState implements UnconnectedInputs {

    private final InitCommand initCommand;
    private final QuitCommand quitCommand;

    public UnconnectedState(ClientConfigPojo clientConfigPojo)
    {
        this.initCommand = new InitCommand();
        this.quitCommand = new QuitCommand();
    }


    @Override
    public Command getInitCommand()
    {
        return initCommand;
    }

    @Override
    public Command getQuitCommand()
    {
        return quitCommand;
    }
}

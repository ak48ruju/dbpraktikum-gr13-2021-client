package States;

import UserInput.Commands.Command;
import UserInput.Executable;

public class SubmitStateState implements SubmitStateInputs {
    private final Command submitCommand;

    public SubmitStateState(Command submitCommand)
    {
        this.submitCommand = submitCommand;
    }

    @Override
    public Command getSubmitCommand()
    {
        return submitCommand;
    }

    public Executable getReturnToConnectedGui()
    {
        return (guiManager, client) -> guiManager.switchGui(guiManager.connectedGui);
    }
}
